#!/usr/bin/env bash

OUTPUT_DEFAULT=jshint_output.txt
OLD_OUTPUT=jshint_old_output.txt
OUTPUT_FILE="${1:-$OUTPUT_DEFAULT}"

node_modules/jshint/bin/jshint --exclude node_modules . > "$OUTPUT_FILE"

DEGRADATION=0

if [ -f $OLD_OUTPUT ]
then
    OLD_SIZE=`wc -l < $OLD_OUTPUT`
    NEW_SIZE=`wc -l < $OUTPUT_FILE`

    if [ $NEW_SIZE -gt $OLD_SIZE ]
    then
        DEGRADATION=1
    fi
fi

mv "$OUTPUT_FILE" $OLD_OUTPUT

exit $DEGRADATION

